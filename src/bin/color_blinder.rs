use std::path::PathBuf;
use std::process::exit;

use color_blinder::{Config, FilterKind, FilterKindParseError, ProcessingStyle};

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
struct Opt {
    /// The input files to process
    #[structopt(required = true, name = "FILE", parse(from_os_str))]
    inputs: Vec<PathBuf>,
    /// Merge all output images into a single big one
    #[structopt(short, long)]
    combine_output: bool,
    #[cfg(feature = "labels")]
    /// Do not add labels to output images
    #[structopt(short, long)]
    obmit_label: bool,
    /// Case in-sensitive selection of the used filters and groups, join multiple selections by comma (eg. 'monochrome,DEUTAN,bvm97')
    #[structopt(short, long)]
    filter: Option<String>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();
    for file in &opt.inputs {
        if file.exists() == false {
            eprintln!("input file {:?} does not exists", file.display());
            exit(1);
        }
    }

    let filters = match opt.filter {
        None => Ok(FilterKind::all()),
        Some(filter) if filter == "?" => {
            show_help();
            exit(2);
        }
        Some(filter) => {
            let filter = filter.trim().split(",").collect::<Vec<_>>();
            FilterKind::from_strs(&*filter)
        }
    };

    let filters = match filters {
        Ok(filters) => {
            if filters.bits().count_ones() == 0 {
                eprintln!("no filters selected!");
                show_help();
                exit(3)
            }
            filters
        }
        Err(FilterKindParseError::UnknownMatches(matches)) => {
            eprintln!("unknown filters: {:?}\n", matches);
            show_help();
            exit(3)
        }
    };

    let pool = threadpool::Builder::new().build();

    let config = Config {
        combine_output: opt.combine_output,
        #[cfg(feature = "labels")]
        render_label: !opt.obmit_label,
        processing: ProcessingStyle::MayOffload(Some(pool.clone())),
    };
    let context = config.into_context();

    for file in &opt.inputs {
        print!("processing {} ... ", file.display());
        context
            .process_file(&file, filters)
            .expect(&*format!("unable to process {}", file.display()));
        println!("\ndone");
    }

    pool.join();

    Ok(())
}

fn show_help() {
    eprintln!("Pick any joined with a comma: ','\n\nFilters:");
    for flag in FilterKind::ALL_FILTERS.iter() {
        eprintln!("    {}", flag.to_str());
    }

    let filter_to_line = |f: FilterKind| f.iter().map(|f| f.to_str()).collect::<Vec<_>>().join(",");
    eprintln!("Groups:");
    eprintln!(
        "    MONOCHROME = {}",
        filter_to_line(FilterKind::MONOCHROME)
    );
    eprintln!("    DEUTAN = {}", filter_to_line(FilterKind::DEUTAN));
    eprintln!("    PROTAN = {}", filter_to_line(FilterKind::PROTAN));
    eprintln!("    TRITAN = {}", filter_to_line(FilterKind::TRITAN));
    eprintln!("    BVM97 = {}", filter_to_line(FilterKind::BVM97));
}

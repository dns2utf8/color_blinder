# color_blinder

[![crates.io](https://img.shields.io/crates/v/color_blinder?logo=rust)](https://crates.io/crates/color_blinder/)
[![CI pipeline](https://gitlab.com/dns2utf8/color_blinder/badges/master/pipeline.svg)](https://gitlab.com/dns2utf8/color_blinder/)

Takes images and renders a set of images simulating different kinds of color blindness.

If you prefer a graphical application [checkout color_blinder_gtk](https://gitlab.com/dns2utf8/color_blinder_gtk)

## Usage

Use the `--combine-output` flag to generate a single output image containing the complete set.

With the `--filter` option you can select which filter should be rendered.
Multiple selections can be joined by comma in any order.
For example: `Deuteranomaly,monochrome,DEUTAN,BVM97`

![Demo image](demo_images/Colours_Simulation_combined.png)

* [x] Achromatomaly
* [x] Achromatopsia
* [x] Deuteranomaly
* [x] Deuteranopia
* [x] DeuteranopiaBVM97
* [x] Protanomaly
* [x] Protanopia
* [x] ProtanopiaBVM97
* [x] Tritanomaly
* [x] Tritanopia
* [x] TritanopiaBVM97

## Installation

Install from [crates.io](https://crates.io/crates/color_blinder) with:
```
cargo install --force color_blinder
```

**Note:** the `--force` tells cargo to install the latest version.

### AUR

`color_blinder` can be installed from available [AUR packages](https://aur.archlinux.org/packages/?O=0&SeB=b&K=color_blinder&outdated=&SB=n&SO=a&PP=50&do_Search=Go) using an [AUR helper](https://wiki.archlinux.org/index.php/AUR_helpers). For example,

```
yay -S color_blinder
```

If you prefer, you can clone the [AUR packages](https://aur.archlinux.org/packages/?O=0&SeB=b&K=color_blinder&outdated=&SB=n&SO=a&PP=50&do_Search=Go) and then compile them with [makepkg](https://wiki.archlinux.org/index.php/Makepkg). For example,

```
git clone https://aur.archlinux.org/color_blinder.git
cd color_blinder
makepkg -si
```

## Features

* [x] Process multiple files in one run
* [x] Unify output images into one
* [x] Optional text labels
    * [x] in the combined image
    * [x] on each image
* [x] Support transparent PNG images
    * [ ] use maximum compression for output files
* [x] Filter by name
    * [x] filter by groups (MONOCHROME, DEUTAN, PROTAN, TRITAN and BVM97)
* [x] Parallel processing per filter
    * [ ] look into optimising parallel processing single images

In case you would like to disable the text feature compile with `cargo build --release --no-default-features`.

# Feedback and/or Pull Request are welcome

# Resources

* http://lpetrich.org/Science/ColorBlindnessSim/ColorBlindnessSim.html
* https://colorblindtools.blogspot.com/2017/02/colorblind-simulation-model-testing.html
* http://www.color-blindness.com/coblis-color-blindness-simulator/

## Online tools

* http://lpetrich.org/Science/ColorBlindnessSim/WebpageCBSim.xhtml

# Changelog

## Unreleased

## 1.0.2

- Add `--filter ?` so people can get a list of the filters
- Update deps

## 1.0.1

- Minor text fixes

## 1.0.0

- Implement filter filters with tests
- Implement parallel processing
- Implement processing strategies Inline, MustOffload and MayOffload
- More tests for calculating positions
- Add more meta-data to Cargo.toml
- Make the crate more useable for consumers
- Output filters on error

## 0.3.2

- Update deps

## 0.3.1

- Fix binary name
- Improve file open speed

## 0.3.0

- Add merge function, create a single output image
- Add labels to optional output images
- Publish demo images
- Refactor the code into a module crate and a bin consumer

## 0.2.1

- Cleanup code

## 0.2.0

- Add Brettel, Vienot and Mollon JOSA 14/10 1997
- Change output filename schema

## 0.1.0

- Initial Release
